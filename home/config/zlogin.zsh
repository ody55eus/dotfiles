#!/usr/bin/env zsh

# Start the SSH Agent and set environment
if command -v ssh-agent &> /dev/null
then
    eval `ssh-agent` && ssh-add
fi
