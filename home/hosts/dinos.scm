(define-module (home hosts dinos)
  #:use-module (rde packages)
  #:use-module (rde features base)
  #:use-module (rde features system)
  #:use-module (rde features wm)
  #:use-module (gnu bootloader)
  #:use-module (gnu bootloader grub)
  #:use-module (gnu home services)
  #:use-module (gnu services)
  #:use-module (gnu services databases)
  #:use-module (gnu system keyboard)
  #:use-module (gnu system file-systems)
  #:use-module (gnu system mapped-devices)
  #:use-module (nongnu system linux-initrd)
  #:use-module (nongnu packages linux)
  #:use-module (ice-9 match))


;;; Hardware/host specifis features
(define custom-services
  (feature-custom-services
   #:feature-name-prefix 'dinos
   #:home-services
   (list
    (simple-service
     'home-sql-extra-packages
     home-profile-service-type
     (list
      (@ (dinos packages virtualization) docker-compose-plugin)
      (@ (gnu packages databases) mariadb))))
   #:system-services
   (list
    (service mysql-service-type)
    )))

;; TODO: Switch from UUIDs to partition labels For better
;; reproducibilty and easier setup.  Grub doesn't support luks2 yet.

(define ixy-file-systems
  (list
   (file-system
     (mount-point "/")
     (device "/dev/sda3")
     (type "btrfs"))
   ;; (file-system
   ;;   (mount-point "/boot/efi")
   ;;   (type "vfat")
   ;;   (device (file-system-label "EFI")))
   ))

(define-public %ixy-features
  (list
   (feature-host-info
    #:host-name "dinos"
    #:issue "This is DinOS. Welcome\n"
    #:locale "en_US.utf8"
    ;; ls `guix build tzdata`/share/zoneinfo
    #:timezone  "Europe/Berlin")
   ;;; Allows to declare specific bootloader configuration,
   ;;; grub-efi-bootloader used by default
   (feature-bootloader
    #:bootloader-configuration (bootloader-configuration
                                (bootloader grub-bootloader)
                                (targets (list "/dev/sda"))
                                (keyboard-layout (keyboard-layout
                                                  "de" "neo"))))
   (feature-file-systems
    ;; #:mapped-devices ixy-mapped-devices
    #:swap-devices (list (swap-space
			   (target "/dev/sda2")))
    #:file-systems   ixy-file-systems)
   (feature-kernel
            #:kernel linux
            #:initrd microcode-initrd
            #:firmware (list linux-firmware))
   ;; (feature-kanshi
   ;;  #:extra-config
   ;;  `((profile laptop ((output DVI-D-1 enable)))
   ;;    (profile docked ((output DVI-D-1 enable)
   ;;                     (output DVI-D-1 scale 2)))))
   (feature-hidpi)
   custom-services))
