(define-module (home hosts hera)
  #:use-module (rde packages)
  #:use-module (rde features base)
  #:use-module (rde features system)
  #:use-module (rde features wm)
  #:use-module (gnu bootloader)
  #:use-module (gnu bootloader grub)
  #:use-module (gnu packages fonts)
  #:use-module (gnu home services)
  #:use-module (gnu services)
  #:use-module (gnu services pam-mount)
  #:use-module (gnu services databases)
  #:use-module (gnu services virtualization)
  #:use-module (gnu services ssh)
  #:use-module (gnu services cups)
  #:use-module (gnu services rsync)
  #:use-module (gnu packages cups)
  #:use-module (gnu system keyboard)
  #:use-module (gnu system file-systems)
  #:use-module (gnu system mapped-devices)
  #:use-module (nongnu system linux-initrd)
  #:use-module (nongnu packages linux)
  #:use-module (guix gexp)
  #:use-module (ice-9 match))

(define pam-mount-rules
  `((debug (@ (enable "0")))
    (volume (@ (user "jp")
               (fstype "ntfs")
               (path "/dev/sdb1")
               (mountpoint "/mnt")))
    (volume (@ (user "jp")
               (fstype "ext4")
               (path "/dev/nvme0n1p2")
               (mountpoint "/media/jp/ubuntu3")))
    (volume (@ (user "jp")
               (fstype "auto")
               (path "/dev/sdc1")
               (mountpoint "/media/jp/book")
               (options "defaults,autodefrag,compress")))
    (mntoptions (@ (allow ,(string-join
                            '("nosuid" "nodev" "loop"
                              "encryption" "fsck" "nonempty"
                              "allow_root" "allow_other")
                            ","))))
    (mntoptions (@ (require "nosuid,nodev")))
    (logout (@ (wait "0")
               (hup "0")
               (term "no")
               (kill "no")))
    (mkmountpoint (@ (enable "1")
                     (remove "true")))))

;;; Hardware/host specifis features
(define custom-services
  (feature-custom-services
   #:feature-name-prefix 'dinos
   #:home-services
   (list
    (simple-service
     'home-hera-extra-packages
     home-profile-service-type
     (list (@ (gnu packages databases) mariadb)
           (@ (dinos packages virtualization) docker-compose-plugin))))
   #:system-services
   (list
    (service qemu-binfmt-service-type
             (qemu-binfmt-configuration
              (platforms (lookup-qemu-platforms "arm" "aarch64"))))
    (service cups-service-type
             (cups-configuration
              (web-interface? #t)
              (extensions
               (list cups-filters
                     (@ (dinos packages cups) gutenprint)))))
    (service mysql-service-type)
    (service openssh-service-type)
    (simple-service
     'system-extra-packages
     profile-service-type
     (list (@ (gnu packages linux) ntfs-3g)
           (@ (gnu packages rsync) rsync)))
    (service rsync-service-type)
    (service pam-mount-service-type
         (pam-mount-configuration
          (rules pam-mount-rules))))))

;; TODO: Switch from UUIDs to partition labels For better
;; reproducibilty and easier setup.  Grub doesn't support luks2 yet.
(define ixy-swap-devices
  (list (swap-space
         (target (uuid "b260eece-7ef3-4433-83f6-3a06051ee8c1")))))

(define ixy-file-systems
  (list (file-system
         (mount-point "/")
         (device (uuid
                  "6032e393-a89c-4906-85be-b0b7af45d4f2"
                  'btrfs))
         (type "btrfs"))
        (file-system
         (mount-point "/boot/efi")
         (device (uuid "CCF5-8953"
                       'fat32))
         (type "vfat"))))

(define-public %ixy-features
  (list
   (feature-host-info
    #:host-name "hera"
    #:issue "This is DinOS. (c) 2023/2024 Jonathan Pieper\n"
    #:locale "en_US.utf8"
    ;; ls `guix build tzdata`/share/zoneinfo
    #:timezone  "Europe/Berlin")
   ;;; Allows to declare specific bootloader configuration,
   ;;; grub-efi-bootloader used by default
   (feature-bootloader
    #:bootloader-configuration (bootloader-configuration
                                (bootloader grub-efi-bootloader)
                                (targets (list "/boot/efi"))
                                (keyboard-layout (keyboard-layout "de" "neo"))))
   (feature-file-systems
    ;; #:mapped-devices ixy-mapped-devices
    #:swap-devices   ixy-swap-devices
    #:file-systems   ixy-file-systems)
   (feature-kernel
    #:kernel linux
    #:initrd microcode-initrd
    #:firmware (list linux-firmware))
   ;; (feature-kanshi
   ;;  #:extra-config
   ;;  `((profile laptop ((output DVI-D-1 enable)))
   ;;    (profile docked ((output DVI-D-1 enable)
   ;;                     (output DVI-D-1 scale 2)))))
   (feature-hidpi
     #:scaling-factor 2
     #:console-font (file-append
                      font-tamzen
                      "/share/kbd/consolefonts/TamzenForPowerline8x16.psf")
    )
   custom-services))
;; Or 5x9 / 6x12 / 7x14 / 8x16 / 10x20
