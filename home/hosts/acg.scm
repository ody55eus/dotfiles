(define-module (home hosts acg)
  #:use-module (rde features base)
  #:use-module (rde features system)
  #:use-module (rde features wm)
  #:use-module (gnu bootloader)
  #:use-module (gnu bootloader grub)
  #:use-module (gnu packages fonts)
  #:use-module (gnu system keyboard)
  #:use-module (gnu system file-systems)
  #:use-module (gnu system mapped-devices)
  #:use-module (nongnu system linux-initrd)
  #:use-module (nongnu packages linux)
  #:use-module (guix gexp)
  #:use-module (ice-9 match))


;;; Hardware/host specifis features

;; TODO: Switch from UUIDs to partition labels For better
;; reproducibilty and easier setup.  Grub doesn't support luks2 yet.

(define ixy-file-systems
  (list
   (file-system
     (mount-point "/")
     (device "/dev/sda3")
     (type "ext4"))
   ;; (file-system
   ;;   (mount-point "/mnt")
   ;;   (type "vboxfs")
   ;;   (device "share"))
   ))

(define-public %ixy-features
  (list
   (feature-host-info
    #:host-name "acg-rde"
    #:locale "en_US.utf8"
    ;; ls `guix build tzdata`/share/zoneinfo
    #:timezone  "Europe/Berlin")
   ;;; Allows to declare specific bootloader configuration,
   ;;; grub-efi-bootloader used by default
   (feature-bootloader
    #:bootloader-configuration (bootloader-configuration
                                (bootloader grub-bootloader)
                                (targets (list "/dev/sda"))
                                (keyboard-layout (keyboard-layout
                                                  "de" "neo"))))
   (feature-file-systems
    ;; #:mapped-devices ixy-mapped-devices
    #:file-systems   ixy-file-systems)
   (feature-kernel
    #:kernel linux
    #:initrd microcode-initrd
    #:firmware (list linux-firmware))
   ;; (feature-kanshi
   ;;  #:extra-config
   ;;  `((profile laptop ((output DVI-D-1 enable)))
   ;;    (profile docked ((output DVI-D-1 enable)
   ;;                     (output DVI-D-1 scale 2)))))
   (feature-hidpi
    ;; #:scaling-factor 1
    ;; #:console-font (file-append
    ;;                  font-tamzen
    ;;                  "/share/kbd/consolefonts/TamzenForPowerline8x16.psf")
    )))
;; Or 5x9 / 6x12 / 7x14 / 8x16 / 10x20
