(define-module (home hosts troja)
  #:use-module (rde features base)
  #:use-module (rde features system)
  #:use-module (rde features wm)
  #:use-module (dinos features finance)
  #:use-module (dinos features backup)
  #:use-module (gnu services shepherd)
  #:use-module (dinos packages python-xyz)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages bootloaders)
  #:use-module (gnu packages certs)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages cryptsetup)
  #:use-module (gnu packages disk)
  #:use-module (gnu packages file-systems)
  #:use-module (gnu packages fonts)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages package-management)
  #:use-module (gnu packages texinfo)
  #:use-module (gnu packages xorg)
  #:use-module (gnu bootloader)
  #:use-module (gnu bootloader grub)
  #:use-module (gnu system keyboard)
  #:use-module (gnu system file-systems)
  #:use-module (gnu system mapped-devices)
  #:use-module (nongnu system linux-initrd)
  #:use-module (nongnu packages linux)
  #:use-module (ice-9 match)
  #:use-module (gnu)
  #:use-module (gnu services networking))


;;; Hardware/host specifis features

;; TODO: Switch from UUIDs to partition labels For better
;; reproducibilty and easier setup.  Grub doesn't support luks2 yet.

(define ixy-file-systems
  (list
   (file-system
     (mount-point "/")
     (device "/dev/sda3")
     (type "btrfs"))
    (file-system
      (mount-point "/boot/efi")
      (type "vfat")
      (device "/dev/sda1"))
   ))


;; These define a service to load the uvesafb kernel module with the
;; appropriate options.  The GUI installer needs it when the machine does not
;; support Kernel Mode Setting.  Otherwise kmscon is missing /dev/fb0.
(define (uvesafb-shepherd-service _)
  (define modprobe
    (program-file "modprobe-wrapper"
                  #~(begin
                      ;; Use a wrapper because shepherd 0.9.3 won't let us
                      ;; pass environment variables to the child process:
                      ;; <https://issues.guix.gnu.org/60106>.
                      (setenv "LINUX_MODULE_DIRECTORY"
                              "/run/booted-system/kernel/lib/modules")
                      (apply execl #$(file-append kmod "/bin/modprobe")
                             "modprobe" (cdr (command-line))))))

  (list (shepherd-service
         (documentation "Load the uvesafb kernel module if needed.")
         (provision '(maybe-uvesafb))
         (requirement '(file-systems))
         (start #~(lambda ()
                    (or (file-exists? "/dev/fb0")
                        (invoke #+modprobe
                                "uvesafb"
                                (string-append "v86d=" #$v86d "/sbin/v86d")
                                "mode_option=1024x768"))))
         (respawn? #f)
         (one-shot? #t))))

(define uvesafb-service-type
  (service-type
   (name 'uvesafb)
   (extensions
    (list (service-extension shepherd-root-service-type
                             uvesafb-shepherd-service)))
   (description
    "Load the @code{uvesafb} kernel module with the right options.")
   (default-value #t)))



(define-public %ixy-features
  (list
   (feature-host-info
    #:host-name "troja"
    #:locale "en_US.utf8"
    ;; ls `guix build tzdata`/share/zoneinfo
    #:timezone  "Europe/Berlin")
   ;;; Allows to declare specific bootloader configuration,
   ;;; grub-efi-bootloader used by default
   (feature-bootloader
    #:bootloader-configuration (bootloader-configuration
                                (bootloader grub-efi-bootloader)
                                (targets (list "/boot/efi"))
                                (keyboard-layout (keyboard-layout
                                                  "de" "neo"
                                                  #:options '("grp:shifts_toggle")))))
   (feature-file-systems
    ;; #:mapped-devices ixy-mapped-devices
    #:file-systems   ixy-file-systems)
   (feature-kernel
            #:kernel linux
            #:kernel-arguments '("console=ttyS0,115200")
            #:initrd microcode-initrd
            #:firmware (list linux-firmware))
   ;; (feature-kanshi
   ;;  #:extra-config
   ;;  `((profile laptop ((output DVI-D-1 enable)))
   ;;    (profile docked ((output DVI-D-1 enable)
   ;;                     (output DVI-D-1 scale 2)))))
   (feature-custom-services
     #:feature-name-prefix 'troja-extra
     #:system-services
     (list
      (service uvesafb-service-type)
      (service wpa-supplicant-service-type)))
   (feature-beancount)
   (feature-beancount-fava
    #:fava-extra-packages (list python-fava-envelope))
   ;; (feature-duply)
   (feature-hidpi)))
