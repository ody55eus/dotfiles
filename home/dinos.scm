(define-module (home dinos)
  #:use-module (home hosts hera)
  #:use-module (dinos features backup)
  #:use-module (dinos features finance)
  #:use-module (dinos features keyboard)
  #:use-module (dinos features emacs)
  #:use-module (dinos features emacs-xyz)
  #:use-module (rde features)
  #:use-module (rde features base)
  #:use-module (rde features gnupg)
  #:use-module (rde features markup)
  #:use-module (rde features security-token)
  #:use-module (rde features keyboard)
  #:use-module (rde features system)
  #:use-module (rde features xdg)
  #:use-module (rde features password-utils)
  #:use-module (rde features emacs)
  #:use-module (rde features emacs-xyz)
  #:use-module (rde features fontutils)
  #:use-module (rde features linux)
  #:use-module (rde features python)
  #:use-module (rde features networking)
  #:use-module (rde features documentation)
  #:use-module (rde features docker)
  #:use-module (rde features gtk)
  #:use-module (rde features image-viewers)
  #:use-module (rde features mail)
  #:use-module (rde features virtualization)
  #:use-module (rde features terminals)
  #:use-module (rde features tmux)
  #:use-module (rde features ssh)
  #:use-module (rde features shells)
  #:use-module (rde features shellutils)
  #:use-module (rde features version-control)
  #:use-module (rde features video)
  #:use-module (rde features wm)
  #:use-module (rde features web-browsers)
  #:use-module (rde features xdisorg)
  #:use-module (contrib features javascript)
  #:use-module (contrib features json)
  #:use-module (contrib features emacs-xyz)

  #:use-module (gnu services)
  #:use-module (gnu home services)
  #:use-module (gnu home services shepherd)
  #:use-module (rde home services i2p)
  #:use-module (rde home services emacs)
  #:use-module (rde home services wm)

  #:use-module (gnu home-services ssh)

  #:use-module (gnu packages)
  #:use-module (rde packages)
  #:use-module (dinos packages nvim)
  #:use-module (dinos packages tmux)
  #:use-module (dinos packages zsh-xyz)

  #:use-module (rde gexp)
  #:use-module (guix gexp)
  #:use-module (guix inferior)
  #:use-module (guix channels)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (ice-9 match))

;;; Helpers
(define* (mail-acc id user #:optional (type 'gmail))
  "Make a simple mail-account with gmail type by default."
  (mail-account
   (id   id)
   (fqda user)
   (type type)))

(define* (mail-lst id fqda urls)
  "Make a simple mailing-list."
  (mailing-list
   (id   id)
   (fqda fqda)
   (config (l2md-repo
            (name (symbol->string id))
            (urls urls)))))

;;; Emacs
(define emacs-extra-packages-service
  (simple-service
   'emacs-extra-packages
   home-emacs-service-type
   (home-emacs-extension
    (elisp-packages
     (append
      (strings->packages
       "emacs-restart-emacs"
       "emacs-piem"
       "emacs-chatgpt-shell"
       "emacs-german-holidays"
       "emacs-ligature"
       "emacs-company-posframe"
       "emacs-yasnippet"
       "emacs-yasnippet-snippets"
       "emacs-consult-yasnippet"
       "emacs-rainbow-delimiters"
       "emacs-ement"
       "emacs-wgrep"
       "emacs-ellama"
       "emacs-org-present"))))))

;;; Packages
(define home-extra-packages-service
  (simple-service
   'home-profile-extra-packages
   home-profile-service-type
   (append
    (list
     (@ (gnu packages tree-sitter) tree-sitter-html)
     (@ (gnu packages tree-sitter) tree-sitter-json)
     (@ (gnu packages tree-sitter) tree-sitter-python)
     (@ (gnu packages tree-sitter) tree-sitter-org)
     (@ (gnu packages guile) guile-next))
    (strings->packages
     "rbw" ;; TODO: Move to feature-bitwarden
     "alacritty"
     "neovim"
     "python-pynvim"
     "python-git-hammer"
     "ghostscript"
     "figlet" ;; TODO: Move to emacs-artist-mode
     ;; "calibre"
     ;; "icecat"
     ;; "ungoogled-chromium-wayland" "ublock-origin-chromium"
     ;; "beancount"
     ;; "python"

     ;; "utox" "qtox" "jami"

     "tmux-tpm"
     "neovim-config"
     "zsh-syntax-highlighting"
     "zsh-autosuggestions"
     "zsh-autopair"
     "zsh-powerlevel10k"

     "flatpak"
     "vlc"
     "alsa-utils" "youtube-dl"
     "pavucontrol" "wev"
     "imagemagick" "gimp"
     ;; "obs" "obs-wlrobs"
     "recutils" "binutils" "make"
     ;; "fheroes2"

     "hicolor-icon-theme" "adwaita-icon-theme" "gnome-themes-extra"
     "papirus-icon-theme" "arc-theme"
     "thunar" "fd"
     ;; "glib:bin"

     ;; "libreoffice"
     "ffmpeg"
     "wl-clipboard-x11"
     "the-silver-searcher" "ripgrep" "curl"))))

;;; Home Folder
(define home-folder-service
  (simple-service 'home-folder-service
                  home-files-service-type
                  (list `(".config/nvim/init.lua"
                          ,(local-file "config/nvim-init.lua" "nvim-init.lua"))
                        `(".config/nvim/lua"
                          ,(file-append neovim-config "/share/lua"))
                        `(".config/alacritty/alacritty.toml"
                          ,(local-file "config/alacritty.toml"))
                        `(".config/zsh/.p10k.zsh"
                          ,(local-file "config/.p10k.zsh" "p10k.zsh"))
                        `(".config/zsh/ohmyzsh"
                          ,(file-append zsh-ohmyzsh "/share/ohmyzsh"))
                        `(".cache/zsh/ohmyzsh/custom/themes"
                          ,(file-append zsh-powerlevel "/share/zsh/plugins/p10k"))
                        `(".cache/zsh/ohmyzsh/custom/plugins/zsh-completions"
                          ,(file-append zsh-completions "/share/zsh/plugins/zsh-completion"))
                        `(".config/tmux/plugins/tpm"
                          ,(file-append tmux-tpm "/share/tmux/plugins/tpm"))
                        `(".config/tmux-powerline/themes/jp.sh"
                          ,(local-file "config/tmux/jp.theme" "tmux-jp.theme"))
                        `(".config/tmux-powerline/config.sh"
                          ,(local-file "config/tmux/.tmux-powerlinerc" "tmux-powerlinerc")))))

;;; Home Environment
(define home-env-service
  (simple-service
   'home-env-settings
   home-environment-variables-service-type
   ;; Make sway work on virtual gpu in qemu
   `(("ZDOTDIR" . "$HOME/.config/zsh")
     ("ZSH" . "$ZDOTDIR/ohmyzsh")
     ("ZSH_CUSTOM" . "$HOME/.cache/zsh/ohmyzsh/custom"))))

(define (wallpaper url hash)
  (origin
    (method url-fetch)
    (uri url)
    (file-name "wallpaper.png")
    (sha256 (base32 hash))))

(define wallpaper-calvin-dark
  (wallpaper "https://raw.githubusercontent.com/lcpz/awesome-copycats/0369445fd7af85e566d324c06b5d0e011b5190f2/themes/powerarrow-dark/wall.png"
             "1aic65353m2ajk34d9ifzmfrba6vvi1jh3n83y3bmmlbwd99r75v"))

;;; Desktop (Sway)
(define sway-extra-config-service
  (simple-service
   'sway-extra-config
   home-sway-service-type
   `(;; (output DVI-D-1 scale 2)
     (output * bg ,wallpaper-calvin-dark center)
     ;; (output eDP-1 disable)
     ,@(map (lambda (x) `(workspace ,x output DVI-D-1)) (iota 8 1))

     ;; (workspace 9 output DP-2)
     ;; (workspace 10 output DP-2)

     ;; (bindswitch --reload --locked lid:on exec /run/setuid-programs/swaylock)

     (bindsym
      --locked $mod+Shift+t exec
      ,(file-append (@ (gnu packages music) playerctl) "/bin/playerctl")
      play-pause)

     (bindsym
      --locked $mod+Shift+n exec
      ,(file-append (@ (gnu packages music) playerctl) "/bin/playerctl")
      next)

     (bindsym $mod+Shift+o move workspace to output left)
     (bindsym $mod+Ctrl+o focus output left)
     (input type:touchpad
            ;; TODO: Move it to feature-sway or feature-mouse?
            (;; (natural_scroll enabled)
             (tap enabled)))

     ;; (xwayland disable)
     ;; (bindsym $mod+Return exec alacritty)
     (bindsym $mod+Shift+Return exec emacs))))

;;; User-specific features with personal preferences
(define sway-wayland-settings-service
  (simple-service
   'sway-wlr-settings
   home-environment-variables-service-type
   ;; Make sway work on virtual gpu
   `(("WLR_RENDERER_ALLOW_SOFTWARE" . "1")
     ("WLR_NO_HARDWARE_CURSORS" . "1"))))

;;; SSH - Network config
(define ssh-extra-config-service
  (simple-service
   'ssh-extra-config
   home-ssh-service-type
   (home-ssh-extension
    (extra-config
     (list
      (ssh-host
       (host "ithaca")
       (options
        '((host-name . "192.168.20.20")
          (port . 22)
          (control-master . "auto")
          (control-path . "~/.ssh/master-%r@%h:%p")
          (compression . #t))))
      (ssh-host
       (host "ody")
       (options
        '((host-name . "192.168.20.102")
          (port . 22)
          (control-master . "auto")
          (control-path . "~/.ssh/master-%r@%h:%p")
          (compression . #t))))
      (ssh-host
       (host "neptun")
       (options
        '((host-name . "192.168.20.27")
          (port . 2123)
          (control-master . "auto")
          (control-path . "~/.ssh/master-%r@%h:%p")
          (compression . #t))))))
    (toplevel-options
     '((host-key-algorithms . "+ssh-rsa")
       (pubkey-accepted-key-types . "+ssh-rsa"))))))

;;; Basic Features
(define %base-features
  (list
   (feature-user-info
    #:user-name "jp"
    #:full-name "Jonathan Pieper"
    #:email "jpieper@mailbox.org"
    #:emacs-advanced-user? #t)
   (feature-gnupg
    #:pinentry-flavor 'qt
    #:default-ttl 86400  ;; 24 hrs
    #:gpg-primary-key "2361DFC839413E7A84B2152B01B6FB927AAEC59B")

   ;; (feature-security-token)
   (feature-mail-settings
    #:mail-accounts (list (mail-acc 'work       "jpieper@mailbox.org" 'mailbox)
                          ;; (mail-acc 'personal   "jonathan-ich-habe-fertig.com" 'gmx-fr)
                          )
    #:mailing-lists (list
                     (mail-lst 'guix-devel "guix-devel@gnu.org"
                               '("https://yhetil.org/guix-devel/0"))
                     (mail-lst 'guix-bugs "guix-bugs@gnu.org"
                               '("https://yhetil.org/guix-bugs/0"))
                     (mail-lst 'guix-patches "guix-patches@gnu.org"
                               '("https://yhetil.org/guix-patches/1"))))
   (feature-password-store
    #:password-store-directory "$HOME/password-store")

   ;; (feature-irc-settings
   ;;  #:irc-accounts (list
   ;;                  (irc-account
   ;;                   (id 'srht)
   ;;                   (network "chat.sr.ht")
   ;;                   (bouncer? #t)
   ;;                   (nick "abcdw"))
   ;;                  (irc-account
   ;;                   (id 'libera)
   ;;                   (network "irc.libera.chat")
   ;;                   (nick "abcdw"))
   ;;                  (irc-account
   ;;                   (id 'oftc)
   ;;                   (network "irc.oftc.net")
   ;;                   (nick "abcdw"))))

   (feature-custom-services
    #:feature-name-prefix 'ody
    #:home-services
    (list
     sway-wayland-settings-service
     emacs-extra-packages-service
     home-extra-packages-service
     home-folder-service
     home-env-service
     sway-extra-config-service
     ssh-extra-config-service
     ;;i2pd-add-ilita-irc-service
     ))

   ;; (feature-ssh-proxy #:host "ithaca" #:auto-start? #f)
   ;; (feature-ssh-proxy #:host "nasserver" #:name "hundredrps"
   ;;                    #:proxy-string "50080:localhost:8080"
   ;;                    #:reverse? #t
   ;;                    #:auto-start? #f)

   (feature-xdg
    #:xdg-user-directories-configuration
    (home-xdg-user-directories-configuration
     (music "$HOME/music")
     (videos "$HOME/vids")
     (pictures "$HOME/pics")
     (documents "$HOME/docs")
     (download "$HOME/dl")
     (desktop "$HOME")
     (publicshare "$HOME")
     (templates "$HOME")))

   ;; (feature-yggdrasil)
   ;; (feature-i2pd
   ;;  #:outproxy 'http://acetone.i2p:8888
   ;;  ;; 'purokishi.i2p
   ;;  #:less-anonymous? #t)

   ;; (feature-javascript)
   ;; (feature-json)

   (feature-notmuch
    #:notmuch-saved-searches
    (cons*
     ;; TODO: Add tag:unread to all inboxes.  Revisit archive workflow.
     '(:name "Work Inbox" :query "tag:work and tag:inbox and tag:unread" :key "W")
     '(:name "Personal Inbox" :query "tag:personal and tag:inbox" :key "P")
     %rde-notmuch-saved-searches)
    #:notmuch-search-oldest-first #f)

   (feature-msmtp)
   (feature-l2md)
   (feature-isync)
   (feature-emacs-message)

   (feature-keyboard
    ;; To get all available options, layouts and variants run:
    ;; cat `guix build xkeyboard-config`/share/X11/xkb/rules/evdev.lst
    #:keyboard-layout
    (keyboard-layout "de" "neo"))

   ;; Web Browser
   (feature-ungoogled-chromium)
   (feature-librewolf)
   ;; (feature-nyxt)
   ;; #:nyxt (@ (gnu packages web-browsers) nyxt))

   ;; Dinos Features
   (feature-duply)          ; Backup
   (feature-beancount)))      ; Finance with Python
   ;;(feature-beancount-fava
   ;; #:fava-extra-packages '()))) ; Web-Interface for Beancount

(define %emacs-features
  (list
   (feature-emacs
    ;;#:emacs (@@ (gnu packages emacs) emacs-next-pgtk)
    #:extra-init-el `((defun dinos/org-setup ()
                        (org-indent-mode 1)  ; Indent text following current headline
                        (variable-pitch-mode 1) ; Enable different Fonts
                        (org-modern-mode 1)
                        (setq-local corfu-auto nil))
                      (add-hook 'org-mode-hook 'dinos/org-setup))
    #:default-application-launcher? #f)

   ;; Appearance
   (feature-emacs-dashboard)
   (feature-emacs-appearance
    #:header-line-as-mode-line? #f)
   (feature-emacs-all-the-icons)
   ;; (feature-emacs-tab-bar)
   ;; (feature-emacs-circadian)             ; Theme-switching based on time of day

   ;; Completion
   (feature-emacs-completion
    #:mini-frame? #f
    #:marginalia-align 'right)
   ;; (feature-emacs-corfu
   ;;  #:corfu-doc-auto #f)
   (feature-emacs-vertico)              ; vertico completion UI
   ;; (feature-emacs-mct)                  ; mct completion UI (only use one completion UI)

   ;; Helpful Tools
   (feature-emacs-help)                 ; Helpful (better help)
   (feature-emacs-info)                 ; info-plus
   (feature-emacs-tramp)                ; Tramp (remote connections, ssh, ftp, etc.)
   ;;(feature-emacs-project)              ; Project
   (feature-emacs-perspective)          ; Perspective (Workspaces)
   (feature-emacs-which-key             ; Which Key (Keybinding Interface)
    #:idle-delay 0.3)
   (feature-emacs-dired)                ; DirEd (Directory Editor)
   ;; (feature-emacs-monocle)              ; Monocle
   (feature-emacs-keycast #:turn-on? #t) ; Keycast (Display Keystrokes and Commands)
   ;; (feature-emacs-ace-window)            ; quickly switch between windows
   ;; (feature-emacs-input-methods)     ; e.g. Cyrillic

   ;; Communication
   ;; (feature-emacs-message)
   ;; (feature-emacs-ebdb)                 ; contact management
   ;; (feature-emacs-elpher)               ; gemini web-browser
   ;; (feature-emacs-erc
   ;;  #:erc-log? #t
   ;;  #:erc-autojoin-channels-alist '((Libera.Chat "#rde")))
   ;; (feature-emacs-telega)
   ;; (feature-emacs-elpher)
   (feature-yt-dlp)
   (feature-emacs-emms)                 ; Emacs MultiMedia System
   (feature-emacs-pulseaudio-control)
   ;; (feature-emacs-webpaste)
   ;; (feature-emacs-display-wttr)

   ;; Reading
   (feature-emacs-pdf-tools)            ; PDF
   (feature-emacs-nov-el)               ; epub
   (feature-emacs-browse-url)
   (feature-emacs-elfeed                ; RSS-Newsreader
    #:elfeed-org-files '("~/ZK/rss.org"))

   ;; Development Tools
   (feature-emacs-xref)                 ; Cross References
   (feature-emacs-re-builder)           ; RegExp Builder
   (feature-emacs-flymake)              ; built-in on-the-fly syntax checker
   (feature-emacs-comint)               ; general command-interpreter-in-a-buffer
   (feature-emacs-eshell)               ; EShell (Emacs Shell with Elisp)
   (feature-emacs-shell)                ; Shell (vanilla Emacs Shell)
   (feature-emacs-graphviz)             ; open source graph visualization software
   (feature-emacs-python)               ; Python

   ;; Lisp Languages
   (feature-emacs-geiser)
   (feature-emacs-elisp)
   (feature-emacs-guix
    #:guix-directory "~/Projects/Code/misc/guix")
   (feature-emacs-eglot)                ; LSP

   (feature-emacs-tempel                ; Text Templates
    #:default-templates? #t
    #:templates
    `(fundamental-mode
      ,#~""
      (t (format-time-string "%Y-%m-%d"))))

   ;; Mail
   ;; (feature-emacs-gnus)

   ;; Notes / References
   ;;(feature-emacs-denote
   ;; #:denote-directory "~/denote")
   (feature-emacs-citation
    #:citar-library-paths (list "~/docs/pdf")
    #:citar-notes-paths (list "~/ZK/References")
    #:global-bibliography (list "~/docs/library.bib"))

   ;; Organization
   (feature-emacs-calc)
   (feature-emacs-time
    #:world-clock-time-format "%A %d %B %R %Z"
    #:display-time? #f
    #:display-time-24hr? #t
    #:world-clock-timezones '(("UTC" "Universal")
                              ("America/Los_Angeles" "Seattle")
                              ("America/Chicago" "Chicago")
                              ("America/New_York" "New York")
                              ("Europe/London" "London")
                              ("Europe/Berlin" "Berlin")
                              ("Europe/Athens" "Athen")
                              ("Asia/Dubai" "Dubai")
                              ("Asia/Calcutta" "Calcutta")
                              ("Asia/Bangkok" "Bangkok")
                              ("Asia/Singapore" "Singapur")
                              ("Australia/Perth" "Perth")
                              ("Asia/Tokyo" "Tokyo")
                              ("Australia/Sydney" "Sydney")))
   (feature-emacs-spelling
    #:spelling-program (@ (gnu packages hunspell) hunspell)
    #:spelling-dictionaries (strings->packages
                             "hunspell-dict-en"
                             "hunspell-dict-de"))
   (feature-emacs-git
    #:project-directory "~/Projects/Code")

   (feature-emacs-calendar
    #:diary-file "~/ZK/daily/diary.org")

   (feature-emacs-org
    #:org-directory "~/ZK"
    #:org-indent? #t
    #:org-todo-keywords '(
                          (sequence "TODO(t)" "EPIC(e)" "PROJ(p)" "|"
                                    "DONE(d)")
                          (sequence "BACKLOG(b)" "NEXT(n)" "PLAN(P)" "ACTIVE(a)"
                                    "REVIEW(r)" "WAIT(W@/!)" "HOLD(h)" "|"
                                    "COMPLETED(c)" "KILL(k)" "CANCELLED(C)" "STOPPED(s@)")
                          )
    #:org-tag-alist '((:startgrouptag . "Sys")
                                        ; Put mutually exclusive tags here
                      ("followup" . ?f)
                      ("recurring" . ?r)
                      ("batch" . ?b)
                      ("planning" . ?p)
                      ("publish" . ?P)
                      (:endgrouptag . "M")
                      (:startgroup . "Dev")
                      ("@sys" . ?S)
                      ("@home" . ?H)
                      ("@work" . ?W)
                      (:endgroup . "S")
                      (:startgroup "Basic")
                      ("@dev" . ?d)
                      ("note" . ?n)
                      ("idea" . ?i)
                      (:endgroup . "S")
                      (:startgroup . "Type")
                      ("ACG" . ?a)
                      ("Private" . ?P)
                      (:endgroup . "S")
                      (:startgroup . "Urgency")
                      ("important" . ?I)
                      ("urgent" . ?U)
                      (:endgroup . "M"))
    #:org-capture-templates
    `(("t" "Todo" entry (file+headline "" "Tasks") ;; org-default-notes-file
       "* TODO %?\nSCHEDULED: %t\n%U\n%a\n" :clock-in t :clock-resume t)))

   (feature-emacs-org-roam
    #:org-roam-todo? #t
    #:org-roam-directory "~/ZK"
    ;; TODO: Rewrite to states
    #:org-roam-capture-templates '(("d" "default" plain
                                    "%?\n\nSee also %a.\n"
                                    :if-new (file+head
                                             "%<%Y%m%d%H%M%S>-${slug}.org"
                                             "#+TITLE: ${title}\n")
                                    :unnarrowed t)
                                   ("j" "Projects" plain
                                    (function jp/read-newproject-template)
                                    :if-new (file+head
                                             "Projects/%<%Y%m%d%H%M%S>-${slug}.org"
                                             "#+TITLE: ${title}\n")
                                    :clock-in :clock-resume
                                    :unnarrowed t)
                                   ("i" "Individuum / Persona" plain
                                    "%?\n\nSee also %a.\n"
                                    :if-new (file+head
                                             "People/%<%Y%m%d%H%M%S>-${slug}.org"
                                             "#+TITLE: ${title}\n")
                                    :unnarrowed t)
                                   ("l" "Literature")
                                   ("ll" "Literature Note" plain
                                    "%?\n\nSee also %a.\n* Links\n- %x\n* Notes\n"
                                    :if-new (file+head
                                             "Literature/%<%Y%m%d%H%M%S>-${slug}.org"
                                             "#+TITLE: ${title}\n")
                                    :unnarrowed t)
                                   ("lr" "Bibliography reference" plain
                                    "#+ROAM_KEY: %^{citekey}\n#+PROPERTY: type %^{entry-type}\n#+FILETAGS: %^{keywords}\n#+AUTHOR: %^{author}\n%?"
                                    :if-new (file+head
                                             "References/${citekey}.org"
                                             "#+TITLE: ${title}\n")
                                    :unnarrowed t)
                                   ("p" "PC" plain
                                    "%?\n\nSee also %a.\n"
                                    :if-new (file+head
                                             "PC/%<%Y%m%d%H%M%S>-${slug}.org"
                                             "#+TITLE: ${title}\n#+date: %U")
                                    :unnarrowed t)))

   (feature-emacs-org-agenda
    #:org-agenda-custom-commands ``(("." "Dashboard"
                                     ((agenda "" ((org-agenda-span 1)
                                                  (org-agenda-scheduled-leaders '("" "Sched.%2dx: "))
                                                  (org-agenda-block-separator nil)
                                                  (org-agenda-entry-types '(:scheduled :timestamp :sexp))
                                                  (org-scheduled-past-days 0)
                                                  ;; We don't need the `org-agenda-date-today'
                                                  ;; highlight because that only has a practical
                                                  ;; utility in multi-day views.
                                                  (org-agenda-day-face-function (lambda (date) 'org-agenda-date))
                                                  ;; (org-agenda-skip-function
                                                  ;;  '(org-agenda-skip-entry-if 'todo '("NEXT")))
                                                  (org-agenda-format-date "%A %-e %B %Y")
                                                  (org-agenda-overriding-header "\nAgenda for the day\n")))
                                      (todo "ACTIVE" ((org-agenda-overriding-header "Active Tasks")))
                                      (todo "NEXT" ((org-agenda-overriding-header "Active Reviews")))
                                      (todo "EPIC" ((org-agenda-overriding-header "Active Epics")))))

                                    ("," "Overview"
                                     ;; TODO: Add A priority to the top.
                                     ((agenda
                                       ""
                                       ((org-agenda-time-grid nil)
                                        (org-agenda-start-on-weekday nil)
                                        (org-agenda-start-day "+1d")
                                        (org-agenda-span 14)
                                        (org-agenda-show-all-dates nil)
                                        (org-agenda-time-grid nil)
                                        (org-agenda-show-future-repeats nil)
                                        (org-agenda-block-separator nil)
                                        (org-agenda-entry-types '(:deadline))
                                        (org-agenda-skip-function '(org-agenda-skip-entry-if 'done))
                                        (org-agenda-overriding-header "\nUpcoming deadlines (+14d)\n")))
                                      (agenda
                                       "*"
                                       ((org-agenda-block-separator nil)
                                        (org-agenda-span 14)
                                        (org-agenda-show-future-repeats nil)
                                        (org-agenda-skip-deadline-prewarning-if-scheduled t)
                                        (org-agenda-overriding-header "\nAgenda\n")))
                                      (alltodo
                                       ""
                                       ((org-agenda-block-separator nil)
                                        (org-agenda-skip-function '(or (org-agenda-skip-if nil '(scheduled))))
                                        (org-agenda-overriding-header "\nBacklog\n")))))

                                    ;; Low-effort next actions
                                    ("E" "Low Effort"
                                     tags-todo "+TODO=\"EPIC\"+Effort<15&+Effort>0"
                                     ((org-agenda-overriding-header "Low Effort Tasks")
                                      (org-agenda-max-todos 20)
                                      (org-agenda-files org-agenda-files)))

                                    ("w" "Workflow Status"
                                     ((todo "WAIT"
                                            ((org-agenda-overriding-header "Waiting on External")
                                             (org-agenda-files org-agenda-files)))
                                      (todo "REVIEW"
                                            ((org-agenda-overriding-header "In Review")
                                             (org-agenda-files org-agenda-files)))
                                      (todo "PLAN"
                                            ((org-agenda-overriding-header "In Planning")
                                             (org-agenda-todo-list-sublevels nil)
                                             (org-agenda-files org-agenda-files)))
                                      (todo "BACKLOG"
                                            ((org-agenda-overriding-header "Project Backlog")
                                             (org-agenda-todo-list-sublevels nil)
                                             (org-agenda-files org-agenda-files)))
                                      (todo "NEXT"
                                            ((org-agenda-overriding-header "Ready for Work")
                                             (org-agenda-files org-agenda-files)))
                                      (todo "ACTIVE"
                                            ((org-agenda-overriding-header "Active Projects")
                                             (org-agenda-files org-agenda-files)))
                                      (todo "COMPLETED"
                                            ((org-agenda-overriding-header "Completed Projects")
                                             (org-agenda-files org-agenda-files)))
                                      (todo "CANC"
                                            ((org-agenda-overriding-header "Cancelled Projects")
                                             (org-agenda-files org-agenda-files)))))
                                    ("h" "Daily habits"
                                     ((agenda ""))
                                     ((org-agenda-show-log t)
                                      (org-agenda-ndays 14)
                                      (org-agenda-log-mode-items '(state))
                                      (org-agenda-skip-function '(org-agenda-skip-entry-if 'notregexp ":DAILY:"))))
                                    ;; other commands here

                                    ("D" "Dashboard 2"
                                     ((agenda "" ((org-deadline-warning-days 4)
                                                  (org-agenda-start-on-weekday 1) ; Monday
                                                  (org-agenda-entry-types '(:scheduled :deadline :timestamp :sexp))
                                                  (org-agenda-show-log t)
                                                  (org-agenda-log-mode-items '(state))
                                                  (org-agenda-show-log 'clockcheck)))
                                      (tags-todo "+PRIORITY=\"A\""
                                                 ((org-agenda-overriding-header "High Priority")))
                                      (tags-todo "followup" ((org-agenda-overriding-header "Needs Follow Up")))
                                      (todo "NEXT"
                                            ((org-agenda-overriding-header "Next Actions")
                                             (org-agenda-max-todos nil)))
                                      (todo "ACTIVE" ((org-agenda-overriding-header "Active Tasks")))
                                      (todo "EPIC" ((org-agenda-overriding-header "Active Epics")))
                                      (todo "BACKLOG"
                                            ((org-agenda-overriding-header "Backlog")
                                             (org-agenda-max-todos 99)))
                                      (todo "REVIEW" ((org-agenda-overriding-header "Active Reviews")))
                                      (todo "HOLD" ((org-agenda-overriding-header "On Hold")))))

                                    ("O" "Monthly Overview"
                                     ((agenda "" ((org-deadline-warning-days 0)
                                                  (org-agenda-show-log 'clockcheck)
                                                  (org-agenda-clockreport-mode t)
                                                  (org-agenda-start-with-clockreport-mode t)
                                                  (org-agenda-clockreport-parameter-plist '(:link nil
                                                                                            :lang "de"
                                                                                            :formular '%
                                                                                            :narrow nil
                                                                                            :filetitle t
                                                                                            :maxlevel 3))
                                                  (org-agenda-start-day "01")
                                                  (org-agenda-span 'month)
                                                  (org-agenda-use-time-grid nil)
                                                  (org-agenda-show-current-time-in-grid nil)
                                                  (org-agenda-log-mode-items '(clock status))
                                                  ))
                                      (todo "ACTIVE" ((org-agenda-overriding-header "Current Tasks")
                                                      (org-agenda-max-todos 5)))
                                      (todo "NEXT" ((org-agenda-overriding-header "Upcoming Tasks")
                                                    (org-agenda-max-todos 5)))))

                                    ("n" "Next Tasks"
                                     ((agenda "" ((org-deadline-warning-days 7)))
                                      (todo "NEXT"
                                            ((org-agenda-overriding-header "Next Tasks")))))

                                    (,(kbd "C-d") "Agenda for the day"
                                     ((agenda
                                       ""
                                       ((org-agenda-span 1)
                                        (org-agenda-scheduled-leaders '("" "Sched.%2dx: "))
                                        (org-agenda-block-separator nil)
                                        (org-agenda-entry-types '(:scheduled :timestamp :sexp))
                                        (org-scheduled-past-days 0)
                                        ;; We don't need the `org-agenda-date-today'
                                        ;; highlight because that only has a practical
                                        ;; utility in multi-day views.
                                        (org-agenda-day-face-function (lambda (date) 'org-agenda-date))
                                        ;; (org-agenda-skip-function
                                        ;;  '(org-agenda-skip-entry-if 'todo '("NEXT")))
                                        (org-agenda-format-date "%A %-e %B %Y")
                                        (org-agenda-overriding-header "\nAgenda for the day\n")))
                                      (todo
                                       "NEXT"
                                       ((org-agenda-block-separator nil)
                                        (org-agenda-overriding-header "\nCurrent Tasks\n")))))

                                    (,(kbd "C-o") "Overview"
                                     ;; TODO: Add A priority to the top.
                                     ((agenda
                                       ""
                                       ((org-agenda-time-grid nil)
                                        (org-agenda-start-on-weekday nil)
                                        (org-agenda-start-day "+1d")
                                        (org-agenda-span 14)
                                        (org-agenda-show-all-dates nil)
                                        (org-agenda-time-grid nil)
                                        (org-agenda-show-future-repeats nil)
                                        (org-agenda-block-separator nil)
                                        (org-agenda-entry-types '(:deadline))
                                        (org-agenda-skip-function '(org-agenda-skip-entry-if 'done))
                                        (org-agenda-overriding-header "\nUpcoming deadlines (+14d)\n")))
                                      (agenda
                                       "*"
                                       ((org-agenda-block-separator nil)
                                        (org-agenda-span 14)
                                        (org-agenda-show-future-repeats nil)
                                        (org-agenda-skip-deadline-prewarning-if-scheduled t)
                                        (org-agenda-overriding-header "\nAgenda\n")))
                                      (alltodo
                                       ""
                                       ((org-agenda-block-separator nil)
                                        (org-agenda-skip-function '(or (org-agenda-skip-if nil '(scheduled))))
                                        (org-agenda-overriding-header "\nBacklog\n")))))))

   ;; DinOS Emacs Features
   (feature-emacs-sane-defaults
    #:longitude 8.8
    #:latitude 50.1)

   (feature-emacs-ag)
   (feature-emacs-tldr)
   (feature-emacs-dinos-appearance)
   (feature-emacs-dinos-eshell)
   (feature-dinos-project)
   ;; (feature-emacs-doom-modeline)
   (feature-emacs-dinos-avy)
   (feature-emacs-dinos-dired)
   (feature-emacs-dinos-perspective)
   (feature-emacs-plantuml)
   (feature-emacs-dinos-roam
    #:bibtex-file-path "~/ZK/BibTeX"
    #:bibtex-completion-library-path '("~/pdf")
    #:bibtex-completion-notes-path "~/ZK/References"
    #:bibtex-completion-bibliography '("~/ZK/BibTeX/Library.bib" "~/ZK/BibTeX/Master.bib")
    #:bibtex-completion-notes-template-multiple-files #f
    #:bibtex-completion-display-formats #f)
   (feature-emacs-dinos-agenda)
   (feature-emacs-ibuffer)
   (feature-dinos-modus-themes
    #:dark? #t)
   (feature-neo)))

(define-public %system-features
  (list
   ;; TODO: merge them into feature-base
   (feature-base-services
    #:default-substitute-urls (list "https://bordeaux.guix.gnu.org"
                                    "https://ci.guix.gnu.org")
    #:guix-substitute-urls (list ;; "https://ci.guix.trop.in"
                                 "https://substitutes.nonguix.org/")
    #:guix-authorized-keys (list (local-file "keys/nonguix.pub")))
   (feature-base-packages)
   (feature-desktop-services)

   (feature-pipewire)
   (feature-backlight #:step 10)
   (feature-networking)))

(define-public %dev-features
  (list
   ;;(feature-python
   ;; #:python (@ (gnu packages python) python-3.12))
   (feature-manpages)
   (feature-tex
    #:extra-tex-packages (list (@ (gnu packages texlive) texlive)))
   (feature-markdown)))

(define-public %virtualization-features
  (list
   (feature-docker)
   (feature-qemu)))

(define-public %cli-features
  (list
   ;; (feature-alacritty
   ;;  ;; TODO: Rename to alacritty-yml
   ;;  ;; #:config-file (local-file "./config/alacritty.toml")
   ;;  #:package (@ (gnu packages terminals) alacritty)
   ;;  #:default-terminal? #f
   ;;  #:backup-terminal? #t
   ;;  #:software-rendering? #t
   ;;  #:extra-config `((window ((opacity . 0.8)))
   ;;                   (colors ((transparent_background_colors . #t)))
   ;;                   (colors.bright ((black . "#575b70")
   ;;                                   (blue . "#caa9fa")
   ;;                                   (cyan . "#9aedfe")
   ;;                                   (green . "#5af78e")
   ;;                                   (magenta . "#ff92d0")
   ;;                                   (red . "#ff6e67")
   ;;                                   (white . "#e6e6e6")
   ;;                                   (yellow . "#f4f99d")))
   ;;                   (colors.normal ((black . "#000000")
   ;;                                   (blue . "#caa9fa")
   ;;                                   (cyan . "#8be9fd")
   ;;                                   (green . "#50fa7b")
   ;;                                   (magenta . "#ff79c6")
   ;;                                   (red . "#ff5555")
   ;;                                   (white . "#bfbfbf")
   ;;                                   (yellow . "#f1fa8c")))
   ;;                   (colors.primary ((background . "#282a36")
   ;;                                    (foreground . "#f8f8f2")))))
   (feature-foot
    #:theme "modus-vivendi")
   (feature-vterm)
   (feature-tmux
    #:tmux-conf (local-file "./config/tmux.conf"))
   (feature-zsh
    #:zshrc `(,(slurp-file-like (local-file "config/zshrc.zsh")))
    #:rde-defaults? #f
    #:default-shell? #t
    #:enable-zsh-autosuggestions? #t)
   (feature-bash)
   (feature-compile)
   (feature-direnv)
   (feature-git)
   (feature-ssh)))

(define-public %ui-features
  (list
   (feature-fonts
    #:font-monospace (font
                      (name "JetBrains Mono")
                      (package (@ (gnu packages fonts) font-jetbrains-mono))
                      (size 12)
                      (weight 'regular))
    #:font-sans (font
                 (name "Iosevka Aile")
                 (package (@ (gnu packages fonts) font-iosevka-aile))
                 (size 12)
                 (weight 'regular))
    #:font-serif (font
                  (name "Iosevka Etoile")
                  (package (@ (gnu packages fonts) font-iosevka-etoile))
                  (size 12)
                  (weight 'regular))
    #:font-unicode (font
                    (name "Noto Emoji")
                    (size 12)
                    (package (@ (rde packages fonts) font-noto-color-emoji)))
    #:default-font-size 12
    #:use-serif-for-variable-pitch? #f
    #:extra-font-packages (list (@ (gnu packages fonts) font-gnu-unifont)
                                (@ (gnu packages fonts) font-liberation)
                                (@ (dinos packages fonts) font-nerd-fonts)))

   ;; https://sr.ht/~tsdh/swayr/
   ;; https://github.com/ErikReider/SwayNotificationCenter
   ;; https://github.com/swaywm/sway/wiki/i3-Migration-Guide

   ;; https://github.com/natpen/awesome-wayland
   (feature-sway
    #:xdg-desktop-portal (@ (gnu packages freedesktop) xdg-desktop-portal-next))
   (feature-sway-run-on-tty
    #:sway-tty-number 2)
   (feature-sway-screenshot
    #:screenshot-key 'q)
   ;; (feature-sway-statusbar
   ;;  #:use-global-fonts? #f)
   (feature-swaynotificationcenter)
   (feature-waybar)
   (feature-rofi
    #:theme "fancy")
   (feature-gtk3
    #:gtk-dark-theme? #t)

   ;; BitTorrent Client
   ;; (feature-transmission #:auto-start? #f)

   (feature-imv)
   ;; (feature-mpv)
   ))

(define-public %all-features
  (append
   %base-features
   %system-features
   %dev-features
   %virtualization-features
   %cli-features
   %ui-features
   %emacs-features))

;;; Resulting Configuration
(define-public ixy-config
  (rde-config
   (features
    (append
     %all-features
     %ixy-features))))

(define-public ixy-os
  (rde-config-operating-system ixy-config))

(define-public ixy-he
  (rde-config-home-environment ixy-config))

(define (dispatcher)
  (let ((rde-target (getenv "TARGET")))
    (match rde-target
      ("ixy-home" ixy-he)
      ("ixy-system" ixy-os)
      (_ ixy-he))))

(dispatcher)
