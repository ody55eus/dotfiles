##
#  Guix System Configuration
#
# @file
# @version 0.1

GUIX_PROFILE=build/profiles/guix
GUIX=./pre-inst-env ${GUIX_PROFILE}/bin/guix
PULL_EXTRA_OPTIONS=
# --allow-downgrades
BUILD_OPTIONS=--substitute-urls="https://ci.guix.gnu.org https://bordeaux.guix.gnu.org"
HOME_DIR=home
#SYS_DIR=home
SYS_DIR=.config/guix/systems
CNF=${HOME_DIR}/dinos.scm
MOUNT_PATH=/mnt
CLEANUP_RANGE=2d
GC_OPTIONS=--delete-generations=${CLEANUP_RANGE}

all: clean rde/build rde/update

guix: build/guix-time-marker

mrproper-sys: clean
	rm -rf build/profiles
	sudo guix system delete-generations ${CLEANUP_RANGE}
	sudo guix gc ${GC_OPTIONS}

mrproper: clean
	rm -rf build/profiles
	guix gc ${GC_OPTIONS}

clean:
	rm -f build/channels-lock.scm build/guix-time-marker

build/profiles:
	mkdir -p build/profiles

build/guix-time-marker: build/channels-lock.scm
	make build/profiles/guix
	touch $@

build/channels-lock.scm: build/channels.scm
	echo -e "(use-modules (guix channels))\n" > ./build/channels-lock-tmp.scm
	guix time-machine -C ./build/channels.scm -- \
	describe -f channels >> ./build/channels-lock-tmp.scm
	mv ./build/channels-lock-tmp.scm ./build/channels-lock.scm

build/profiles/guix: build/profiles build/channels-lock.scm
	guix pull -C build/channels-lock.scm -p ${GUIX_PROFILE} ${PULL_EXTRA_OPTIONS}

hera/system/build: guix
	TARGET="ixy-system" ${GUIX} system build ${BUILD_OPTIONS} ${CNF}

hera/system/install: guix
	sudo TARGET="ixy-system" ${GUIX} system reconfigure ${BUILD_OPTIONS} ${CNF}

hera/home/build: guix
	TARGET="ixy-home" ${GUIX} home build ${BUILD_OPTIONS} ${CNF}

hera/home/install: guix
	TARGET="ixy-home" ${GUIX} home reconfigure ${BUILD_OPTIONS} ${CNF}

home/build: guix
	${GUIX} home build ${BUILD_OPTIONS} ${HOME_DIR}/home.scm

home/install: home/build
	${GUIX} home reconfigure ${BUILD_OPTIONS} ${HOME_DIR}/home.scm

live/build: guix
	TARGET=ixy-live ${GUIX} system build ${BUILD_OPTIONS} ${CNF}

live/iso: guix
	TARGET=ixy-live ${GUIX} system image -t iso900 ${BUILD_OPTIONS} ${CNF}

system/init: guix
	sudo TARGET=ixy-system ${GUIX} system init ${BUILD_OPTIONS} ${CNF} ${MOUNT_PATH}

system/build: guix
	${GUIX} system build ${BUILD_OPTIONS} ${CNF}

system/install: system/build
	sudo ${GUIX} system reconfigure ${BUILD_OPTIONS} ${CNF}

rde/home/build: guix
	TARGET=ixy-home ${GUIX} home build ${BUILD_OPTIONS} ${CNF}

rde/home/install: guix
	TARGET=ixy-home ${GUIX} home reconfigure ${BUILD_OPTIONS} ${CNF}

rde/home/test: rde/home/install
	emacs --debug-init

nas/build: guix
	${GUIX} system build ${BUILD_OPTIONS} ${SYS_DIR}/neptun.scm

nas/install: guix
	sudo ${GUIX} system reconfigure ${BUILD_OPTIONS} ${SYS_DIR}/neptun.scm

rde/sys/build: guix
	TARGET=ixy-system ${GUIX} system build ${BUILD_OPTIONS} ${CNF}

rde/sys/install: guix
	sudo TARGET=ixy-system ${GUIX} system reconfigure ${BUILD_OPTIONS} ${CNF}

rde/sys/vm: rde/sys/build
	TARGET=ixy-system ./image.sh ${CNF}

rde/update: rde/home/install rde/sys/install
	echo "Successfully updated RDE/DinOS"

rde/build: rde/home/build rde/sys/build
	echo "Successfully build RDE/DinOS"

worker/vm: guix
	./image.sh ${SYS_DIR}/worker-vm.scm

help:
	@echo "Usage: make mrproper | clean | rde/[home|sys|update]?/[build/install|init]"
	@echo
	@echo "mrproper         | Deletes old profiles and channels-lock"
	@echo "clean            | Delete channels-lock"
	@echo "rde/home/build   | Building Home Environment"
	@echo "rde/home/install | Reconfiguring Home Environment"
	@echo "rde/sys/build    | Building System Environment"
	@echo "rde/sys/install  | Reconfiguring System Environment"
	@echo "rde/sys/init     | Initializing a new System Environment"
	@echo "rde/build        | Building Home and System Environment"
	@echo "rde/upgrade      | Reconfiguring Home and System Environment"
