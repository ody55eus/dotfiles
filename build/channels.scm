(list (channel
       (name 'guix)
       (url "https://git.savannah.gnu.org/git/guix.git")
       (branch "master")
       (introduction
        (make-channel-introduction
         "9edb3f66fd807b096b48283debdcddccfea34bad"
         (openpgp-fingerprint
          "BBB0 2DDF 2CEA F6A8 0D1D  E643 A2A0 6DF2 A33A 54FA"))))
      (channel
       (name 'dinos)
       (url "https://gitlab.com/ody55eus/dinos")
       (branch "main")
       (introduction
        (make-channel-introduction
         "e802e65a76e49435ded5ef909120b2b84f2e262b"
         (openpgp-fingerprint
          "885B 941B 8221 3321 6D96 0E4C DE2A D6CF 2474 B880")))))
