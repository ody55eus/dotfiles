(use-modules (guix channels))

(list (channel
        (name 'guix)
        (url "https://git.savannah.gnu.org/git/guix.git")
        (branch "master")
        (commit
          "2bb8ecbfbf20efb126670ec92a7cdd88a4805902")
        (introduction
          (make-channel-introduction
            "9edb3f66fd807b096b48283debdcddccfea34bad"
            (openpgp-fingerprint
              "BBB0 2DDF 2CEA F6A8 0D1D  E643 A2A0 6DF2 A33A 54FA"))))
      (channel
        (name 'dinos)
        (url "https://gitlab.com/ody55eus/dinos")
        (branch "main")
        (commit
          "baca20073045fe74cebcc12033df3f10f9859cf7")
        (introduction
          (make-channel-introduction
            "e802e65a76e49435ded5ef909120b2b84f2e262b"
            (openpgp-fingerprint
              "885B 941B 8221 3321 6D96  0E4C DE2A D6CF 2474 B880"))))
      (channel
        (name 'nonguix)
        (url "https://gitlab.com/nonguix/nonguix")
        (branch "master")
        (commit
          "d0b6099324cd47961064bd96ae830c4ca6d1bada")
        (introduction
          (make-channel-introduction
            "897c1a470da759236cc11798f4e0a5f7d4d59fbc"
            (openpgp-fingerprint
              "2A39 3FFF 68F4 EF7A 3D29  12AF 6F51 20A0 22FB B2D5"))))
      (channel
        (name 'rde)
        (url "https://github.com/abcdw/rde")
        (branch "master")
        (commit
          "ad31febff72ccb214db88aadc5d34e22d35d7a48")
        (introduction
          (make-channel-introduction
            "257cebd587b66e4d865b3537a9a88cccd7107c95"
            (openpgp-fingerprint
              "2841 9AC6 5038 7440 C7E9  2FFA 2208 D209 58C1 DEB0")))))
