#!/bin/sh

START_DATE=${1:-$(date -I --date="last monday")}

emacs -batch -eval "(org-batch-agenda \"D\" org-agenda-span (quote week)   org-agenda-include-diary nil org-agenda-clockreport-mode t   org-agenda-start-with-clockreport-mode t    org-agenda-clockreport-parameter-plist (list :link nil :lang \"de\" :narrow nil :filetitle t :hidefiles t :maxlevel 3) org-agenda-start-day \"${START_DATE}\")"
