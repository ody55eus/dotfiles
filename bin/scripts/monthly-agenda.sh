#!/bin/sh

START_DATE=${1:-$(date +%Y-%m-01)}

emacs -batch -eval "(org-batch-agenda \"a\" org-agenda-span (quote month)   org-agenda-include-diary nil org-agenda-clockreport-mode t   org-agenda-start-with-clockreport-mode t    org-agenda-clockreport-parameter-plist (list :link nil :lang \"de\" :narrow nil :filetitle t :hidefiles t :maxlevel 3) org-agenda-start-day \"${START_DATE}\")"
