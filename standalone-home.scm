;; [[file:../Guix.org::*Basic Home config][Basic Home config:1]]
(use-modules (gnu home)
             (gnu home services)
             (gnu home services shells)
             (gnu services)
             (guix gexp))

(home-environment
 (packages (specifications->packages (list
                                      ;; Default Terminal Tools
                                      "zsh"
                                      "zsh-syntax-highlighting"
                                      "zsh-autopair"
                                      "zsh-autosuggestions"
                                      "tmux"
                                      "direnv"
                                      "gwl"
                                      "git"
                                      "git-flow"
                                      "bat"
                                      "fzf"
                                      "gnupg"
                                      "openssh"
                                      "net-tools"
                                      "make"
                                      "glibc-locales"
                                      "the-silver-searcher"  ; Better Ripgrep

                                      ;; Virtualization
                                      "docker"
                                      "docker-compose"

                                      ;; Editors
                                      "neovim"

                                      ;; X-Tools
                                      "picom"
                                      "redshift"
                                      "brightnessctl"
                                      "xdg-utils"      ;; For xdg-open, etc
                                      "xdg-dbus-proxy" ;; For Flatpak
                                      "gtk+:bin"       ;; For gtk-launch
                                      "glib:bin"       ;; For gio-launch-desktop
                                      "shared-mime-info"
                                      "xset"
                                      "xrandr"         ; Screen-Resolution
                                      "xsel"           ; Manipulate Selections
                                      "dmenu"          ; Menu Launcher
                                      "rofi"           ; Application Launcher
                                      "pinentry"       ; X11-Password Entry

                                      "alacritty"
                                      "vlc"
                                      "mpv"
                                      "youtube-dl"
                                      "playerctl"
                                      "gimp"
                                      "gucharmap"
                                      "fontmanager"

                                      "gstreamer"
                                      "gst-plugins-base"
                                      "gst-plugins-good"
                                      "gst-plugins-bad"
                                      "gst-plugins-ugly"
                                      "gst-libav"
                                      "intel-vaapi-driver"
                                      "libva-utils"

                                      "feh"
                                      "gimp"
                                      "scrot"          ; CLI Screenshots
                                      "ghostscript"

                                      ;; Themes
                                      "adwaita-icon-theme"
                                      "papirus-icon-theme"
                                      "hicolor-icon-theme"
                                      "matcha-theme"

                                      ;; Fonts
                                      "font-juliamono"
                                      "font-jetbrains-mono"
                                      "font-font-awesome"
                                      "font-nerd-fonts"

                                      ;; E-Mail
                                      "isync"
                                      "mu"

                                      ;; Self Defined
                                      "zsh-ohmyzsh"
                                      "zsh-completions"
                                      "awesome-copycats"
                                      "tmux-tpm"

                                      ;; Emacs
                                      "emacs-pgtk"

                                      ;; NeoVim
                                      ;; "neovim-lunarvim" ; TODO: not working
                                      "neovim-config"
                                      )))
 (services
  (list (service
         home-bash-service-type
         (home-bash-configuration
          (aliases
           '(("l" . "ls -CF")
             ("la" . "ls -A")
             ("vi" . "nvim")
             ("wget" . "wget -c")
             ("lsd" . "ls -lAF | grep --color=never '^d'")
             ("df" . "df -h")
             ("psmem" . "ps aux | sort -nr -k 4 | head -5")
             ("pscpu" . "ps aux | sort -nr -k 3 | head -5")
             ("gpg-check" . "gpg --keyserver-options auto-key-retrieve --verify")
             ("gpg-retrieve" . "gpg --keyserver-options auto-key-retrieve --receive-keys")
             ("mergepdf" . "gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=_merged.pdf")
             ("path" . "echo -e \"${PATH//:/\\n}\"")
             ("ips" . "grep -o 'inet6\\? \\(addr:\\)\\?\\s\\?\\(\\(\\([0-9]\\+\\.\\)\\{3\\}[0-9]\\+\\)\\|[a-fA-F0-9:]\\+\\)' | awk '{ sub(/inet6? (addr:)? ?/, \\\"\\\"); print }'")
             ("ll" . "ls -l")))
          (bashrc
           (list (local-file "./config/.bashrc" "bashrc")))))
        (service
         home-zsh-service-type
         (home-zsh-configuration
          (environment-variables
           '(("ZDOTDIR" . "$HOME/.config/zsh")
             ("ZSH" . "$HOME/.config/zsh/ohmyzsh")
             ("ZSH_CUSTOM" . "$HOME/.cache/zsh/ohmyzsh/custom")
             ("HISTFILE" . "$ZDOTDIR/.zsh_history")
             ("HISTSIZE" . "1000000")
             ("SAVEHIST" . "500000")
             ("MANWIDTH" . "999")
             ("EDITOR" . "\"emacsclient -t -a 'nvim'\"")
             ("VISUAL" . "\"emacsclient -c -a 'emacs'\"")
             ("MANPAGER" . "\"nvim -c 'Man!' -o -\"")
             ("PYTHONENCODING" . "UTF-8")
             ("LANG" . "en_US.UTF-8")
             ("LC_ALL" . "en_US.UTF-8")
             ("GUIX_LOCPATH" . "$HOME/.guix-home/profile/lib/locale")
             ("GPG_TTY" . "$(tty)")
             ("KEYTIMEOUT" . "1")
             ))
          (zshrc
           (list (local-file "./config/.zshrc" "zshrc")
                 (let ((commit "d41ca84af1271e8bfbe26f581cebe3b86521d0db")
                       (origin
                        (method git-fetch)
                        (uri (git-reference
                              (url "https://github.com/ohmyzsh/ohmyzsh.git/")
                              (commit commit)))
                        (file-name "ohmyzsh")
                        (sha256
                         (base32
                          "0kj252ywhc0jw0j7mr3dwx4q5mi5rrlm4jlhc8mbx66ylfvxi9qg")))))
                 )))))))
;; Basic Home config:1 ends here
